﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Parameters
{

	private Dictionary<string, char> charData;
	private Dictionary<string, string> stringData;

	private Dictionary<string, int> intData;
	private Dictionary<string, float> floatData;
	private Dictionary<string, short> shortData;
	private Dictionary<string, long> longData;
	private Dictionary<string, double> doubleData;

	private Dictionary<string, bool> boolData;

	private Dictionary<string, ArrayList> arrayListData;
	private Dictionary<string, object> objectListData;

	public Parameters ()
	{
		this.charData = new Dictionary<string, char> ();
		this.stringData = new Dictionary<string, string> ();

		this.intData = new Dictionary<string, int> ();
		this.floatData = new Dictionary<string, float> ();
		this.shortData = new Dictionary<string, short> ();
		this.longData = new Dictionary<string, long> ();
		this.doubleData = new Dictionary<string, double> ();

		this.boolData = new Dictionary<string, bool> ();

		this.arrayListData = new Dictionary<string, ArrayList> ();
		this.objectListData = new Dictionary<string, object> ();
	}

	public void PutExtra (string parameterName, char value)
	{
		this.charData.Add (parameterName, value);
	}

	public void PutExtra (string parameterName, string value)
	{
		this.stringData.Add (parameterName, value);
	}

	public void PutExtra (string parameterName, int value)
	{
		this.intData.Add (parameterName, value);
	}

	public void PutExtra (string parameterName, float value)
	{
		this.floatData.Add (parameterName, value);
	}

	public void PutExtra (string parameterName, short value)
	{
		this.shortData.Add (parameterName, value);
	}

	public void PutExtra (string parameterName, long value)
	{
		this.longData.Add (parameterName, value);
	}

	public void PutExtra (string parameterName, double value)
	{
		this.doubleData.Add (parameterName, value);
	}

	public void PutExtra (string parameterName, bool value)
	{
		this.boolData.Add (parameterName, value);
	}

	public void PutExtra (string parameterName, ArrayList arrayList)
	{
		this.arrayListData.Add (parameterName, arrayList);
	}

	public void PutExtra (string parameterName, object[] objectArray)
	{
		ArrayList arrayList = new ArrayList ();
		arrayList.AddRange (objectArray);
		this.PutExtra (parameterName, arrayList);
	}

	public void PutObjectExtra (string parameterName, object value)
	{
		this.objectListData.Add (parameterName, value);
	}

	public char GetCharExtra (string paramName, char defaultValue)
	{
		if (this.charData.ContainsKey (paramName))
			return this.charData[paramName];

		else
			return defaultValue;
	}

	public string GetStringExtra (string paramName, string defaultValue)
	{
		if (this.stringData.ContainsKey (paramName))
			return this.stringData[paramName];

		else
			return defaultValue;
	}

	public int GetIntExtra (string paramName, int defaultValue)
	{
		if (this.intData.ContainsKey (paramName))
			return this.intData[paramName];

		else
			return defaultValue;
	}

	public float GetFloatExtra (string paramName, float defaultValue)
	{
		if (this.floatData.ContainsKey (paramName))
			return this.floatData[paramName];

		else
			return defaultValue;
	}

	public short GetShortExtra (string paramName, short defaultValue)
	{
		if (this.shortData.ContainsKey (paramName))
			return this.shortData[paramName];

		else
			return defaultValue;
	}

	public long GetLongExtra (string paramName, long defaultValue)
	{
		if (this.longData.ContainsKey (paramName))
			return this.longData[paramName];

		else
			return defaultValue;
	}

	public double GetDoubleExtra (string paramName, double defaultValue)
	{
		if (this.doubleData.ContainsKey (paramName))
			return this.doubleData[paramName];

		else
			return defaultValue;
	}

	public bool GetBoolExtra (string paramName, bool defaultValue)
	{
		if (this.boolData.ContainsKey (paramName))
			return this.boolData[paramName];

		else
			return defaultValue;
	}

	public ArrayList GetArrayListExtra (string paramName)
	{
		if (this.arrayListData.ContainsKey (paramName))
			return this.arrayListData[paramName];

		else
			return null;
	}

	public object[] GetArrayExtra (string paramName)
	{
		ArrayList arrayListData = this.GetArrayListExtra (paramName);

		if (arrayListData != null)
			return arrayListData.ToArray ();

		else
			return null;
	}

	public object GetObjectExtra (string paramName)
	{
		if (this.objectListData.ContainsKey (paramName))
			return this.objectListData[paramName];

		else
			return null;
	}
}
