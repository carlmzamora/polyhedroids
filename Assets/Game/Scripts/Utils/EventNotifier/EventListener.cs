﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EventListener
{
	private static EventListener instance;

	public Dictionary<string, ObserverList> eventObservers;

	public static EventListener Instance
	{
		get
		{
			if (instance == null)
				instance = new EventListener ();

			return instance;
		}
	}

	private EventListener ()
	{
		this.eventObservers = new Dictionary<string, ObserverList> ();
	}

	public void AddObserver (string notifName, System.Action<Parameters> action)
	{
		if (this.eventObservers.ContainsKey (notifName))
		{
			ObserverList eventObserver = this.eventObservers[notifName];
			eventObserver.AddObserver (action);
		}

		else
		{
			ObserverList eventObserver = new ObserverList ();
			eventObserver.AddObserver (action);
			this.eventObservers.Add (notifName, eventObserver);
		}
	}

	public void AddObserver (string notifName, System.Action action)
	{
		if (this.eventObservers.ContainsKey (notifName))
		{
			ObserverList eventObserver = this.eventObservers[notifName];
			eventObserver.AddObserver (action);
		}

		else
		{
			ObserverList eventObserver = new ObserverList ();
			eventObserver.AddObserver (action);
			this.eventObservers.Add (notifName, eventObserver);
		}
	}

	public void RemoveObserver (string notifName)
	{
		if (this.eventObservers.ContainsKey (notifName))
		{
			ObserverList eventObserver = this.eventObservers[notifName];
			eventObserver.RemoveAllObservers ();
			this.eventObservers.Remove (notifName);
		}
	}

	public void RemoveActionAtObserver (string notifName, System.Action action)
	{
		if (this.eventObservers.ContainsKey (notifName))
		{
			ObserverList eventObserver = this.eventObservers[notifName];
			eventObserver.RemoveObserver (action);
		}
	}

	public void RemoveActionAtObserver (string notifName, System.Action<Parameters> action)
	{
		if (this.eventObservers.ContainsKey (notifName))
		{
			ObserverList eventObserver = this.eventObservers[notifName];
			eventObserver.RemoveObserver (action);
		}
	}

	public void RemoveAllObservers ()
	{
		foreach (ObserverList eventObserver in this.eventObservers.Values)
		{
			eventObserver.RemoveAllObservers ();
		}

		this.eventObservers.Clear();
	}

	public void PostEvent (string notifName)
	{
		if (this.eventObservers.ContainsKey (notifName))
		{
			ObserverList eventObserver = this.eventObservers[notifName];
			eventObserver.NotifyObservers ();
		}
	}

	public void PostEvent (string notifName, Parameters parameters)
	{
		if (this.eventObservers.ContainsKey (notifName))
		{
			ObserverList eventObserver = this.eventObservers[notifName];
			eventObserver.NotifyObservers (parameters);
		}
	}
}
