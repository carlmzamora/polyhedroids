﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDataManager : Manager
{
	private PlayerDataManager instance;
	public PlayerDataManager Instance
	{
		get { return this; }
	}
	void Awake ()
	{
		if (instance == null)
			instance = this;
		if (instance != this)
			Destroy (this.gameObject);
	}

	public override void Initialize ()
	{
		base.Initialize ();
	}
}