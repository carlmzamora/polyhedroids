﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimersKeys
{
	public const string PLAYER_NITRO_TIMER = "playerNitroTimer";
}

public enum TimerCountMode
{
	COUNTDOWN,
	COUNTUP
}

public enum TimerPlaybackType
{
	ONCE,
	LOOP
}

public class TimersManager : MonoBehaviour
{
	private Dictionary<string, Timer> timers = new Dictionary<string, Timer> ();
	public Dictionary<string, Timer> Timers
	{
		get { return timers; }
	}

	private List<string> toDeleteKeys = new List<string> ();
	private Dictionary<string, Timer> postUpdateTimers = new Dictionary<string, Timer> ();

	private static TimersManager instance;
	public static TimersManager Instance
	{
		get { return instance; }
	}
	void Awake ()
	{
		if (instance == null)
			instance = this;
		if ( instance != this)
			Destroy (this.gameObject);
	}

	void Update ()
	{
		if (postUpdateTimers.Count > 0)
			timers = postUpdateTimers;

		foreach (string key in timers.Keys)
		{
			timers[key].Count ();
		}

		if (toDeleteKeys.Count > 0)
		{
			for (int i = 0; i < toDeleteKeys.Count; i++)
			{
				timers.Remove (toDeleteKeys[i]);
			}

			postUpdateTimers = timers;
			toDeleteKeys = new List<string> ();
		}
	}

	public void DeleteTimer (string timerKey)
	{
		toDeleteKeys.Add (timerKey);
	}
}

public class Timer
{
	private float duration = 0;
	private Action action;
	private bool actPerTick;
	private float tickTime;
	private Action tickAction;
	private TimerPlaybackType pbType;
	private TimerCountMode cMode;

	private float countProgress;
	public string CountProgessText
	{
		get {
			TimeSpan timespan = TimeSpan.FromSeconds (countProgress);
			return (timespan.Hours.ToString ("00") + ":" + timespan.Minutes.ToString ("00") +
				":" + timespan.Seconds.ToString ("00") + "." + ((int)(timespan.Milliseconds / 10)).ToString ("00"));
		}
	}

	private float oldTime;
	private float newTime;

	private float tickProgress;

	private bool isCounting = false;
	private bool hasFinished = false;

	public Timer (float duration, Action action, bool actPerTick = false, float tickTime = 0, Action tickAction = null, TimerPlaybackType type = TimerPlaybackType.ONCE, TimerCountMode mode = TimerCountMode.COUNTDOWN)
	{
		if (duration <= 0)
		{
			Debug.LogError ("Duration cannot be 0!");
			return;
		}

		this.duration = duration;
		this.action = action;
		this.actPerTick = actPerTick;
		this.tickAction = tickAction;
		this.tickTime = tickTime;
		this.pbType = type;
		this.cMode = mode;
	}

	public void Begin ()
	{
		if (duration <= 0)
		{
			Debug.LogWarning ("A timer has no duration!");
		}

		if (!isCounting)
		{
			isCounting = true;
			this.newTime = Time.time;

			if (cMode == TimerCountMode.COUNTDOWN)
			{
				countProgress = duration;
				tickProgress = tickTime;
			}
			else if (cMode == TimerCountMode.COUNTUP)
			{
				countProgress = 0;
				tickProgress = 0;
			}
		}
	}

	public void Count ()
	{
		if (isCounting)
		{
			if (cMode == TimerCountMode.COUNTDOWN)
			{
				oldTime = newTime;
				newTime = Time.time;

				if (this.actPerTick)
				{
					float tickTimeDifference = newTime - oldTime;
					if ((tickProgress - tickTimeDifference) <= 0)
					{
						this.tickAction ();
						tickProgress = tickTime;
					}
					tickProgress -= tickTimeDifference;
				}

				float timeDifference = newTime - oldTime;
				if ((countProgress - timeDifference) <= 0)
				{
					Stop ();

					this.action ();
				}
				countProgress -= timeDifference;
			}
		}
	}

	public float Stop ()
	{
		if (isCounting)
			isCounting = false;

		return countProgress;
	}

	public void Resume ()
	{
		if (!isCounting)
		{
			isCounting = true;
			this.newTime = Time.time;
		}
	}

	public void Reset ()
	{
		Stop ();
		this.newTime = Time.time;
		countProgress = duration;
	}
}