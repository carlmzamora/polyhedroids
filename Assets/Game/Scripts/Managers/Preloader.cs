﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Preloader : MonoBehaviour
{
	public Transform managers;
	private int requiredManagers;
	private int initializedManagers = 0;

	public delegate void LoadDoneDelegate ();
	public static event LoadDoneDelegate OnLoadDone;

	void Start ()
	{
		Manager.OnLoad += CheckManagersInitialization;

		requiredManagers = managers.childCount;
		DontDestroyOnLoad (managers.gameObject);

		for (int i = 0; i < requiredManagers; i++)
		{
			Manager manager = managers.GetChild (i).GetComponent<Manager> ();
			manager.Initialize ();
		}
	}

	private void CheckManagersInitialization ()
	{
		initializedManagers++;

		if (initializedManagers > requiredManagers)
			Debug.LogWarning ("Initialized manager more than required!");
		if( initializedManagers >= requiredManagers)
		{
			if (OnLoadDone != null)
				OnLoadDone ();	//subscribe commands to execute to this
		}
	}
}

public class Manager : MonoBehaviour
{
	public delegate void LoadDelegate ();
	public static event LoadDelegate OnLoad;

	public virtual void Initialize ()
	{
		OnLoad ();
	}
}