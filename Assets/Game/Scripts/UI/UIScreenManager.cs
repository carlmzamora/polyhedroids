﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScreenManager : MonoBehaviour
{
	private static UIScreenManager instance;
	public static UIScreenManager Instance
	{
		get {
			if (instance == null)
			{
				instance = new UIScreenManager ();
			}
			return instance;
		}
	}

	[SerializeField] private GameObject uiRoot;

	[SerializeField] private string screenPrefabLocation = "Prefabs/UI/Screens";
	[SerializeField] private string firstScreen;
	[SerializeField] private GameObject overlay;

	private Dictionary<string, GameObject> loadedPrefabs = new Dictionary<string, GameObject> ();
	private Dictionary<string, AScreen> loadedScreens = new Dictionary<string, AScreen> ();

	public List<string> loadedScreenNames = new List<string> ();
	public List<AScreen> activeScreens = new List<AScreen> ();
	public List<string> screenStacks = new List<string> ();

	void Awake ()
	{
		instance = this;
	}

	void Start ()
	{
		Show (firstScreen);
        this.HideOverlay ();
	}

	#region OVERLAY FUNCTIONS

    public void ShowOverlay ()
	{
        if (this.overlay == null)
		{
            return;
        }
        this.overlay.GetComponent<RectTransform> ().SetAsLastSibling ();
        this.overlay.SetActive (true);
    }

    public void HideOverlay ()
	{
        if(this.overlay == null)
		{
            return;
        }
        this.overlay.SetActive (false);
    }

	#endregion

	public GameObject GetScreenPrefab (string screenName)
	{
		if (loadedPrefabs.ContainsKey (screenName))
		{
			return loadedPrefabs [screenName];
		}

		UnityEngine.Object obj = Resources.Load (screenPrefabLocation + "/" + screenName);
		if (obj == null)
		{ 
			return null;
		}

		GameObject gObj = obj as GameObject;
		loadedPrefabs.Add (gObj.name, gObj);

		return gObj;
	}

	public AScreen GetActiveScreen (string screenName)
	{
		for (int i = 0; i < activeScreens.Count; i++)
		{
			if (activeScreens [i].name == screenName)
			{
				return activeScreens [i];
			}
		}
		return null;
	}

	public List<AScreen> GetActiveScreenList ()
	{
		return activeScreens;
	}

	public void Show (string screenName)
	{
		if (GetActiveScreen (screenName) != null || GetScreenPrefab (screenName) == null || uiRoot == null)
		{
			return;
		}

		AScreen uiScreen;
		if (!loadedScreens.ContainsKey (screenName))
		{
			GameObject gObj = GameObject.Instantiate (GetScreenPrefab (screenName)) as GameObject;
			gObj.GetComponent<RectTransform> ().SetParent (uiRoot.transform, false);

			gObj.GetComponent<RectTransform> ().localScale = Vector3.one;
			gObj.GetComponent<RectTransform> ().localPosition = Vector3.zero;
			gObj.GetComponent<RectTransform> ().localRotation = Quaternion.identity;
			gObj.name = screenName;
			gObj.SetActive (false);

			uiScreen = gObj.GetComponent<AScreen> ();
			loadedScreens.Add (screenName, uiScreen);
			if (loadedScreenNames.Contains (screenName))
			{
				int loadedScreenNameIndex = loadedScreenNames.FindIndex (x => x == screenName);
				loadedScreenNames.RemoveAt (loadedScreenNameIndex);
			}

			loadedScreenNames.Add (screenName);
		}
		else
		{
			uiScreen = loadedScreens [screenName];
		}

		uiScreen.Show ();
	}

	public void ShowHidden (string screenName)
	{
		if (GetActiveScreen (screenName) != null || GetScreenPrefab (screenName) == null || uiRoot == null)
		{
			return;
		}

		AScreen uiScreen = loadedScreens[screenName];

		if (!uiScreen.IsModal)
		{
			ClearScreen ();
		}

		if (uiScreen.IsRootScreen)
		{
			ClearStacks (screenName);
		}

		if (screenStacks.Contains (screenName))
		{
			screenStacks.Remove (screenName);
		}

		if(uiScreen.IsSwitch)
		{
			//this is for top stack only
			AScreen topStack = activeScreens [0];
			//Debug.Log (topStack.ScreenName);
			if(topStack.IsSwitch)
			{
				topStack.OnBack (); // this instead of forcebackhidden so Onback will be called and all listeners will be removed accordingly.
				//this.ForceBackHidden (topStack.ScreenName);
			}

			//does not work. error due to list getting edited while incrementing from it
			/*foreach (AScreen screen in activeScreens)
			{
				if (screen.IsSwitch)
				{
					this.ForceBackHidden (screen.ScreenName);
				}
			}*/
		}

		activeScreens.Insert (0, uiScreen);
		screenStacks.Insert (0, screenName);

		uiScreen.gameObject.SetActive (true);
		if (uiScreen.GetComponent<Animator> () != null)
		{
			uiScreen.GetComponent<Animator> ().SetTrigger ("Intro");
		}
	}

	/*public void ForceHide(string screenName, Action hideCallback = null){
		if(!loadedScreens.ContainsKey(screenName)){
			return;
		}

		AScreen uiScreen = loadedScreenNames [screenName];

		if (uiScreen.GetComponent<Animator> () != null) {
			StartCoroutine (DelayHideScene (uiScreen.gameObject, hideCallback));
		}

		else {
			uiScreen.gameObject.SetActive (false);
			if(hideCallback != null){
				hideCallback ();
			}

			if(!uiScreen.IsRootScreen){
				GameObject.Destroy (uiScreen.gameObject);
			}
		}


	}*/

	public void Hide (string screenName)
	{
		Hide (screenName, null);
	}

	public void Hide (string screenName, Action hideCallback)
	{
		if (!loadedScreens.ContainsKey (screenName))
		{
			return;
		}

		AScreen uiScreen = loadedScreens[screenName];
		//loadedScreens.Remove(screenName);
		activeScreens.Remove (uiScreen);
		if (uiScreen.GetComponent<Animator> () != null)
		{
			StartCoroutine (DelayHideScene (uiScreen.gameObject, hideCallback));
		}
		else
		{
			uiScreen.gameObject.SetActive (false);
			if (hideCallback != null) hideCallback ();
		}

		if (!uiScreen.IsRootScreen)
		{
			GameObject.Destroy (uiScreen.gameObject);
		}
	}

	IEnumerator DelayHideScene (GameObject panelObject, Action hideCallback)
	{
		Animator currentAnimator = panelObject.GetComponent<Animator> ();
		currentAnimator.SetTrigger ("Outro");

		while (!currentAnimator.GetCurrentAnimatorStateInfo(0).IsName ("IdleOutro"))
		{
			yield return null;
		}

		panelObject.SetActive (false);

		if (hideCallback != null)
		{
			hideCallback ();
		}
	}

	public bool IsVisible (string screenName)
	{
		for (int i = 0; i < activeScreens.Count; i++)
		{
			if (activeScreens[i].ScreenName == screenName) return true;
		}

		return false;
	}

	public void Back ()
	{
		if (activeScreens.Count > 0)
		{
			activeScreens [0].OnBack ();
		}
	}

	public T GetScreenComponent<T> () where T : Component
	{
		if (activeScreens.Count == 0)
		{
			return default (T);
		}

		return activeScreens[0].gameObject.GetComponent<T> ();
	}

	public void BackHidden ()
	{
		if (screenStacks.Count > 1)
		{
			Hide (screenStacks[0], () => {
					screenStacks.RemoveAt (0);
					Show (screenStacks[0]);
				});
		}
	}

	public void ForceBackHidden (string screenName)
	{
		if (screenStacks.Count > 1)
		{
			if (screenName != "")
			{
				int stackIndex = screenStacks.FindIndex (x => x == screenName);

				if (stackIndex != 0)
				{
					screenStacks.RemoveAt (stackIndex);
					AScreen ascreen = activeScreens[stackIndex];
					activeScreens.RemoveAt (stackIndex);

					screenStacks.Insert (0, screenName);
					activeScreens.Insert (0, ascreen);
				}
			}

			/*else {
				Hide (screenName, () => {
					screenStacks.RemoveAt(stackIndex);
					Show(screenStacks[0]);
				});
			}*/

			this.BackHidden ();
		}
	}

	public void ExecuteHideCallback ()
	{
		if (screenStacks.Count > 1)
		{
			screenStacks.RemoveAt (0);
			Show (screenStacks[0]);
		}
	}

	public void ClearStacks (string screenName)
	{
		screenStacks.Clear ();
		for (int i = 0; i < loadedScreenNames.Count; i++)
		{
			if (!loadedScreenNames[i].Equals(screenName) && loadedScreens.ContainsKey(loadedScreenNames[i]))
			{
				AScreen uiScreen = loadedScreens[loadedScreenNames[i]];
				loadedScreens.Remove (uiScreen.ScreenName);
				Destroy (uiScreen.gameObject);
				loadedScreenNames.RemoveAt (i);
				i--;
			}
		}
	}

	void ClearScreen ()
	{
		while (activeScreens.Count > 0)
		{
			activeScreens[0].Hide ();
		}
	}

	public void Remove (AScreen uiScreen)
	{
		loadedScreens.Remove (uiScreen.gameObject.name);
		if (activeScreens.Contains (uiScreen))
		{ 
			activeScreens.Remove (uiScreen);
		}
	}
}

[RequireComponent (typeof (CanvasGroup))]
public abstract class AScreen : MonoBehaviour
{
	[SerializeField] protected bool isModal = false;
	[SerializeField] protected bool isRootScreen = false;
	[SerializeField] protected bool isSwitch = false;

	[SerializeField] private AnimationClip enterClip;
	[SerializeField] private AnimationClip exitClip;

	private Animation assignedAnimation;
	private bool isExiting = false;

	private const string CLIP_ENTER_NAME = "ClipEnter";
	private const string CLIP_EXIT_NAME = "ClipExit";

	public string ScreenName
	{
		get{ return gameObject.name; }
	}

	public bool IsModal
	{
		get { return isModal; }
	}

	public bool IsRootScreen
	{
		get { return isRootScreen; }
	}

	public bool IsSwitch
	{
		get { return this.isSwitch; }
	}

	protected virtual void OnDestroy ()
	{
		if (UIScreenManager.Instance != null)
		{ 
			UIScreenManager.Instance.Remove (this);
		}
	}

	public virtual void Show ()
	{
		UIScreenManager.Instance.ShowHidden (ScreenName);
		if(this.enterClip != null)
		{
			this.PlayEnterAnim ();
		}

	}

	public virtual void Hide ()
	{
		UIScreenManager.Instance.Hide (ScreenName);
	}

	public virtual void Hide (System.Action hideCallback)
	{
		UIScreenManager.Instance.Hide (ScreenName, hideCallback);
	}

	public virtual void OnBack ()
	{
		if (this.exitClip != null && this.isExiting == false)
		{
			StartCoroutine ("PlayExitAnim");
			this.isExiting = true;
		}
		else if (this.isExiting == false)
		{
			if (this.isSwitch)
			{
				UIScreenManager.Instance.ForceBackHidden (ScreenName);
			}

			else
			{
				UIScreenManager.Instance.BackHidden ();
			}
		}
	}

	private void PlayEnterAnim ()
	{
		//attach enter animation on the fly
		this.VerifyAnimationComponent ();
		this.AttachClipIfNeeded (this.enterClip, CLIP_ENTER_NAME);
		this.GetComponent<Animation> ().Play (CLIP_ENTER_NAME);
	}

	private IEnumerator PlayExitAnim()
	{
		//attach exit animation on the fly
		this.VerifyAnimationComponent ();
		this.AttachClipIfNeeded (this.exitClip, CLIP_EXIT_NAME);
		this.GetComponent<Animation> ().Play (CLIP_EXIT_NAME);

		UIScreenManager.Instance.ExecuteHideCallback ();
		while (this.GetComponent<Animation> ().isPlaying)
		{
			yield return null;
		}
		UIScreenManager.Instance.Hide (ScreenName);

	}

	private void VerifyAnimationComponent ()
	{
		if (this.assignedAnimation == null)
		{
			this.assignedAnimation = this.gameObject.AddComponent<Animation> ();
		}
	}

	private void AttachClipIfNeeded (AnimationClip clip, string clipName)
	{
		if(this.assignedAnimation.GetClip (clipName) == null)
		{
			this.assignedAnimation.AddClip (clip, clipName);
		}
	}
}