﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenNames
{
	public const string LOADING_SCREEN = "LoadingScreen";
	public const string START_SCREEN = "StartScreen";
	public const string TOPBAR = "TopBar";
	public const string HOME_SCREEN = "HomeScreen";
	public const string SHIP_SELECT_SCREEN = "ShipSelectScreen";
}
