﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class HomeScreen : AScreen, IPointerClickHandler
{
	#region IPointerClickHandler implementation
	public void OnPointerClick (PointerEventData eventData) {}
	#endregion

	public override void Show ()
	{
		base.Show ();

		TopBar topbar = UIScreenManager.Instance.GetActiveScreen (ScreenNames.TOPBAR) as TopBar;
		if (topbar != null)
		{
			topbar.BackButton.interactable = false;
		}
	}

	public override void OnBack ()
	{
		base.OnBack ();
	}

	public void OnNewGameButtonPress ()
	{
		UIScreenManager.Instance.Show (ScreenNames.SHIP_SELECT_SCREEN);
	}
}
