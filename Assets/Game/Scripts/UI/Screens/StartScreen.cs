﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class StartScreen : AScreen, IPointerClickHandler
{
	#region IPointerClickHandler implementation
	public void OnPointerClick (PointerEventData eventData) {}
	#endregion

	public override void Show ()
	{
		base.Show ();
	}

	public override void OnBack ()
	{
		base.OnBack ();
	}

	public void OnTapButtonPress ()
	{
		UIScreenManager.Instance.Show (ScreenNames.TOPBAR);
	}
}
