﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class LoadingScreen : AScreen, IPointerClickHandler
{
	#region IPointerClickHandler implementation
	public void OnPointerClick (PointerEventData eventData) {}
	#endregion

	[Header ("Loading Screen")]
	[SerializeField] private Slider loadingBar;

	protected AsyncOperation asyncLoad = null;
	protected bool sessionHasBegun = false;

	void Awake ()
	{
		Preloader.OnLoadDone += StartLoad;
	}

	public override void Show ()
	{
		base.Show ();
	}

	public override void OnBack ()
	{
		base.OnBack ();
	}

	#region LEVEL LOADING
	void StartLoad ()
	{
		StartCoroutine (LoadLevel ());
	}

	private IEnumerator LoadLevel ()
	{
		asyncLoad = SceneManager.LoadSceneAsync ("MainMenu");

		while (!asyncLoad.isDone)
		{
			loadingBar.value = asyncLoad.progress * 100f;
			yield return new WaitForEndOfFrame ();
		}
		asyncLoad = null;

		sessionHasBegun = true;
		while (!sessionHasBegun)
		{
			yield return new WaitForEndOfFrame ();
		}
		Destroy (this.gameObject);
	}
	#endregion
}
