﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using TMPro;

public class TopBar : AScreen, IPointerClickHandler
{
	#region IPointerClickHandler implementation
	public void OnPointerClick (PointerEventData eventData) {}
	#endregion

	[Header ("TopBar")]
	[SerializeField] private Button backButton;
	public Button BackButton
	{
		get { return backButton; }
	}

	[SerializeField] private TextMeshProUGUI spacePartsCounter;

	public override void Show ()
	{
		base.Show ();
		UIScreenManager.Instance.Show (ScreenNames.HOME_SCREEN);
	}

	public override void OnBack ()
	{
		if (UIScreenManager.Instance.GetActiveScreen (ScreenNames.SHIP_SELECT_SCREEN) != null)
		{
			UIScreenManager.Instance.Show (ScreenNames.HOME_SCREEN);
			return;
		}
			
		base.OnBack ();
	}

	public void OnGetMoreSpacePartsButtonPress ()
	{
	}

	public void OnSettingsButtonPress ()
	{
		
	}
}
