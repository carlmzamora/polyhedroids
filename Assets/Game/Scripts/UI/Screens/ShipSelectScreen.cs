﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using TMPro;

public class ShipSelectScreen : AScreen, IPointerClickHandler
{
	#region IPointerClickHandler implementation
	public void OnPointerClick (PointerEventData eventData) {}
	#endregion

	[Header ("Ship Select Screen")]
	[SerializeField] private Transform shipPreviewContainer;
	[SerializeField] private TextMeshProUGUI shipName;
	[SerializeField] private TextMeshProUGUI skillName;
	[SerializeField] private TextMeshProUGUI infoText;
	[SerializeField] private Transform statsContainer;
	[SerializeField] private Transform skillsContainer;
	[SerializeField] private Transform shipsContainer;
	[SerializeField] private GameObject statUIReference;
	[SerializeField] private GameObject skillUIReference;
	[SerializeField] private GameObject shipUIReference;

	public override void Show ()
	{
		base.Show ();

		TopBar topbar = UIScreenManager.Instance.GetActiveScreen (ScreenNames.TOPBAR) as TopBar;
		if (topbar != null)
		{
			topbar.BackButton.interactable = true;
		}
	}

	public override void OnBack ()
	{
		base.OnBack ();
	}

	public void OnShipNamePress ()
	{
		
	}

	public void OnSkillTreeButtonPress ()
	{
		
	}

	public void OnStartGamePress ()
	{
		SceneManager.LoadScene ("Game");
	}
}
